﻿/* Part of nuget package */
function Site(options) {
    "use strict";

    var self = this;

    var globalScope = {};
    var defaultOptions = {
        getNodeScope: function() { return globalScope },
        isDebug: true,
        contentSectionSelector: "section#content",
        onBeforeSectionUpdate: function () { },
        modulePrefix: "site",
        modulesRoot: '/Scripts/Modules/',
        modules: {}
    };
    var o = $.extend({}, defaultOptions, options);

    var resolver = new Resolver(o.isDebug);
    var notifications = new Emitter();
    resolver.globalServiceInstance("resolver", resolver);
    resolver.globalServiceInstance("site", self);
    resolver.globalServiceInstance("events", notifications);
    resolver.service("scropeEvents", Emitter);
    
    var scopeInitializers = [];
    var scopeFinalizers = [];
    var modules = [];
    var responseHandlers = [];
    var navigationHandlers = [];
    var ajaxHandlers = [];

    //#region Module Loading

    var makeModuleJsName = function (moduleName) {

        var out = moduleName.replace(/^\s*/, "");  // strip leading spaces
        out = out.replace(/^[a-z]|[^\s][A-Z]/g, function (str, offset) {
            if (offset == 0) {
                return str;
            } else {
                return str.substr(0, 1) + "-" + str.substr(1);
            }
        });

        return o.modulePrefix + out.toLowerCase() + ".js";
    };

    var requireJsPromices = {};
    var requireJsAsync = function (jsFileName) {
        if (requireJsPromices[jsFileName]) {
            return requireJsPromices[jsFileName];
        }

        var deffered = $.Deferred();

        var result = requireJsPromices[jsFileName] = deffered.promise();

        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.async = true;
        script.onload = script.onreadystatechange = function (_, isAbort) {
            if (!script.readyState || /loaded|complete/.test(script.readyState)) {
                if (isAbort) {
                    deffered.reject();
                    throw new Error("Can't load JS from URL: " + jsFileName);
                } else
                    deffered.resolve();
            }
        };
        script.onerror = function () {
            deffered.reject();
            throw new Error("Can't load JS from URL: " + jsFileName);
        };
        script.src = jsFileName + (o.isDebug ? "" : ("?" + (new Date).getTime()));
        document.getElementsByTagName('head')[0].appendChild(script);

        return result;
    }

    var jsModules = {};
    var loadModuleAsync = function (moduleName) {
        var jsFileName = o.modules[moduleName] || o.modulesRoot + makeModuleJsName(moduleName);
        var result = requireJsAsync(jsFileName);
        result.done(function () {
            jsModules[moduleName] = jsFileName;
        });
        return result;
    };
    
    var callModuleAsync = function (context, moduleName, functionName, functionParameters) {
        var scope = o.getNodeScope(context);

        var callModuleMethod = function () {
            var service = window[moduleName];
            if (!service) {
                throw new Error("Can't find object 'window." + moduleName + "'");
            }

            resolver.service(moduleName.toLowerCase(), service, service.$inject);
            var module = resolver.get(moduleName.toLowerCase(), scope);

            if (!module[functionName]) {
                throw new Error("Can't find function '" + functionName + "' in object window.'" + moduleName + "'");
            }

            return module[functionName].apply(context, functionParameters);
        };

        if (jsModules[moduleName]) {
            //just call it
            return callModuleMethod();
        }
        else {
            //wait and call
            loadModuleAsync(moduleName).done(callModuleMethod);
            return void 0;
        }
    };

    var callFunctionInternal = function (context, functionName, functionParameters) {
        try {

            var functionParts = functionName.split('.');
            if (functionParts.length != 2) {
                throw new Error("Function name '" + functionName + "' is incorrect. It should be in format 'Module.method'");
            }

            var objName = functionParts[0];
            var funcName = functionParts[1];

            return callModuleAsync(context, objName, funcName, functionParameters);

        } catch (e) {
            window.onerror(null, null, null, null, e);
        }

        return void 0;
    };

    var callFunction = function (context, functionName, functionParameters) {
        var functions = functionName.split(',');
        for (var i = 0; i < functions.length; i++) {
            callFunctionInternal(context, functions[i], functionParameters);
        }
    }

    var applyScripts = function (selector, $scope, additionalModules, onModulesLoaded) {
        var i;

        var scripts = $scope.find(selector).addBack().filter(selector).toArray();
        for (i = 0; i < scripts.length; i++) {
            scripts[i] = {
                script: scripts[i],
                index: i,
                order: $(scripts[i]).data("script-order") || 0
            };
        }

        scripts.sort(function compare(a, b) {
            if (a.order < b.order) {
                return -1;
            }
            if (a.order > b.order) {
                return 1;
            }
            // a must be equal to b
            return a.index - b.index;
        });

        //pre-load all required modules
        var requiredModules = [];
        $.each(scripts, function () {
            var script = this.script;

            var functionNames = $(script).data("value").split(",");
            for (i = 0; i < functionNames.length; i++) {
                var functionName = functionNames[i];
                var functionParts = functionName.split('.');
                var dependencies = $(script).data("modules");
                if (functionName && functionParts.length == 2) {
                    requiredModules.push(functionParts[0]);
                }
                if (dependencies) {
                    dependencies = dependencies.split(',');
                    for (var j = 0; j < dependencies.length; j++) {
                        requiredModules.push(dependencies[j]);
                    }
                }
            };
        });

        var modulesAjax = additionalModules || [];
        $(".ajax-init-modules", $scope).each(function () {
            var modules = $(this).data("modules");
            if (modules) {
                var modulesToLoad = modules.split(",");
                for (i = 0; i < modulesToLoad.length; i++) {
                    modulesAjax.push(loadModuleAsync(modulesToLoad[i]));
                }
            }
        });

        for (i = 0; i < requiredModules.length; i++) {
            modulesAjax.push(loadModuleAsync(requiredModules[i]));
        }

        var deffered = $.Deferred();
        $.when.apply($, modulesAjax).then(function () {
            if (onModulesLoaded) onModulesLoaded();
            //run in required order
            $.each(scripts, function () {
                var script = this.script;
                var functionNames = $(script).data("value").split(",");
                for (i = 0; i < functionNames.length; i++) {
                    var functionName = functionNames[i];
                    callFunction(script, functionName);
                }
            });
            deffered.resolve();
        }, function(ex) {
            deffered.reject(ex);
        });

        return deffered;
    };

    var applyInitScripts = function (scope) {
        return applyScripts(".ajax-init-script", scope, null, function() {
            for (var i = 0; i < scopeInitializers.length; i++) {
                resolver.callResolved(scopeInitializers[i].initFunction, scope, null, scopeInitializers[i].parameters);
            }
        });
    };

    var applyUnloadScripts = function (scope) {
        return applyScripts(".ajax-unload-script", scope, null,  function() {
            for (var i = 0; i < scopeFinalizers.length; i++) {
                resolver.callResolved(scopeFinalizers[i].finalizeFunction, scope, null, scopeFinalizers[i].parameters);
            }
        });
    };

    var startModules = function () {
        var promises = [];
        for (var i = 0; i < modules.length; i++) {
            var module = modules[i];
            var moduleInstance;
            if (module.name) {
                moduleInstance = resolver.get(module.name);
            } else {
                moduleInstance = resolver.resolve(module.func, null, module.parameters);
            }
            if (moduleInstance && moduleInstance.$deffered && 'function' === typeof moduleInstance.$deffered.then) {
                promises.push(moduleInstance.$deffered);
            }
        }
        notifications.publish("modules.started");
        return promises;
    };

    //#endregion

    //#region Request filtration

    var currentAjaxRequestId = "";
    var newAjaxRequestId = function () {
        currentAjaxRequestId = Math.random().toString(32).slice(2);
        return currentAjaxRequestId;
    };

    var checkRequestId = function (id) {
        return id == currentAjaxRequestId;
    };

    var syncAjax = function (options, disableRequestFilter) {
        if (!disableRequestFilter) {
            options.requestId = newAjaxRequestId();
            var successHandler = options.success;
            var completeHandler = options.complete;
            if (successHandler) {
                options.success = function (data) {
                    if (checkRequestId(options.requestId)) {
                        successHandler.apply(this, [data]);
                    }
                };
            }
            options.complete = function (data) {
                if (completeHandler) {
                    if (checkRequestId(options.requestId)) {
                        completeHandler.apply(this, [data]);
                    }
                }
            };
        }

        return $.ajax(options);
    };

    var oldAjax = $.ajax;
    $.ajax = function (options) {
        if (typeof options != "string") {
            options.headers = $.extend({}, options.headers, { "SPA-Referer": window.location.toString() });
        }
        return oldAjax.apply(this, arguments);
    }

    //#endregion
    
    //#region SPA

    var applyPageScripts = function($scope) {
        applyInitScripts($scope).done(function () {
            notifications.publish("page.ready");
        });
    };
    
    var start = function () {
        var $body = $("body");
        applyPageScripts($body)
        o.onBeforeSectionUpdate($body);
    };

    var updateBlock = function(data, options) {
        if (data instanceof jQuery) {
            if (!(options.target instanceof jQuery))
                throw new Error("Target should be jQuery object!");

            applyUnloadScripts(options.target);

            o.onBeforeSectionUpdate(data);

            if (options.replaceTarget) {
                options.target.replaceWith(data);
                applyPageScripts(data);
            } else {
                options.target.html(data);
                applyPageScripts(options.target);
            }

            return data;
        } else {
            throw new Error("Invalid response: it's not HTML and not handled by response handlers! Response: " + JSON.stringify(data));
        }
    };

    var processAjaxData = function (data, options) {

        if (typeof data == "string") {
            data = $(data);
        }

        if (options.responseFilter) {
            data = options.responseFilter(data);
        }

        for (var i = 0; i < responseHandlers.length; i++) {
            var result = responseHandlers[i].apply(options.context, [data, options]);
            if (result) return result;
        }

        return updateBlock(data, options);
    };

    var currentPath;
    var disableHistoryChange = false;
    var modifyHistoryState = function(url, title, replaceState) {
        disableHistoryChange = true;
        try {
            var action = replaceState ? "replaceState" : "pushState";
            history[action]({}, title || window.title, url);
        } finally {
            disableHistoryChange = false;
        }
        currentPath = options.url;

        return true;
    };

    var loadPage = function (options) {

        options = $.extend({
            url: null,
            method: "GET",
            data: null,
            context: null,
            initiator: null,
            target: $(o.contentSectionSelector),
            responseFilter: null,
            replaceTarget: false,
            disableHistory: false,
            stop: false
        }, options);

        for (var i = 0; i < navigationHandlers.length; i++) {
            var result = navigationHandlers[i].apply(this, [currentPath, options]);
            if (result) return;
        }
        
        var data = options.data || [];
        if (options.method.toUpperCase() == "GET") {
            data = $.param(data);
        }

        if (options.stop) return;
        
        var ajaxOptions = {
            data: data,
            url: options.url,
            method: options.method,
            success: function(data) {
                if (data) {
                    if (typeof data === "string" && data.indexOf("<html>") > -1) {
                        throw new Error("Attempt to load full page in AJAX mode!");
                    }

                    if (currentPath != options.url && options.method.toUpperCase() == "GET" && !options.disableHistory) {
                        modifyHistoryState(options.url, document.title);
                    }

                    processAjaxData(data, options);
                }
            }
        };

        for (i = 0; i < ajaxHandlers.length; i++) {
            var result = ajaxHandlers[i].apply(this, [ajaxOptions]);
            if (result) return;
        }

        syncAjax(ajaxOptions);
    };

    // ReSharper disable once Html.EventNotResolved
    window.addEventListener('popstate', function () {
        if (disableHistoryChange)
            return;

        currentPath = location.pathname + location.search;
        
        loadPage({
            url: currentPath,
            initiator: "history"
        });
    }, false);

    $(document).on("click", "a.ajax-view", function (e) {
        e.preventDefault();

        var link = $(this);
        var url = link.attr("href");
        if (url === "#") return;
        
        loadPage({
            context: link,
            url: url,
            initiator: "link"
        });
    });

    //#endregion

    //#region Start
    
    var startSite = function() {
        $.when.apply($, arguments).then(function () {
            var promises = startModules();
            $.when.apply($, promises).then(function () {
                start();
            });
        });
    }

    //#endregion
    
    //#region Public interface

    this.start = startSite;
    this.module = function () {
        var name = null;
        var moduleFunction = null;
        var parameters = null;
        for (var i = 0; i < arguments.length; i++) {
            if (typeof (arguments[i]) === "string") {
                if (name) throw new Error("Name presents twice in module definition!");
                name = arguments[i];
            }
            if (Array.isArray(arguments[i])) {
                if (parameters) throw new Error("Parameters present twice in module definition!");
                parameters = arguments[i];
            }
            if (typeof (arguments[i]) === "function") {
                if (moduleFunction) throw new Error("Function presents twice in module definition!");
                moduleFunction = arguments[i];
            }
        }

	if (name) resolver.service(name, moduleFunction, parameters, true);

        modules.push({
            name: name,
            func: moduleFunction,
            parameters: parameters
        });

        return {
            asSingleton: function() {
                resolver.singleton(name, moduleFunction, parameters, true);
            }
        };
    };
    this.addHtmlInitializer = function (parameters, initFunction) {
        if (typeof (parameters) === "function") {
            initFunction = parameters;
            parameters = null;
        }
        scopeInitializers.push({
            initFunction: initFunction,
            parameters: parameters
        });
    };
    this.addHtmlFinalizer = function (parameters, finalizeFunction) {
        if (typeof (parameters) === "function") {
            finalizeFunction = parameters;
            parameters = null;
        }
        scopeFinalizers.push({
            finalizeFunction: finalizeFunction,
            parameters: parameters
        });
    };
    this.addResponseHandler = function (handler) {
        responseHandlers.push(handler);
    };
    this.addNavigationHandler = function (handler) {
        navigationHandlers.push(handler);
    };
    this.addAjaxHandler = function (handler) {
        ajaxHandlers.push(handler);
    };
    this.requireJsAsync = requireJsAsync;
    this.applyInitScripts = applyInitScripts;
    this.applyUnloadScripts = applyUnloadScripts;
    this.callFunction = callFunction;
    this.syncAjax = syncAjax;
    this.loadPage = loadPage;
    this.processAjaxData = processAjaxData;
    this.updateBlock = updateBlock;
    this.pushHistoryState = function(url, title) {
        modifyHistoryState(url, title);
    };
    this.replaceHistoryState = function (url, title) {
        modifyHistoryState(url, title, true);
    };
    this.getOptions = function () { return o; }
    this.getResolver = function () { return resolver; }

    //#endregion
}