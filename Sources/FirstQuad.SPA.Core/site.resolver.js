﻿/* Part of nuget package */
function Resolver(checkFunctionNames) {
    "use strict";

    var self = this;
    var serviceDescriptions = {};
    var globalScope = {};
    var globalServiceDescriptions = {};

    //http://stackoverflow.com/questions/1007981/how-to-get-function-parameter-names-values-dynamically-from-javascript
    var stripComments = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
    var argumentNames = /([^\s,]+)/g;
    function getParamNames(func) {
        var fnStr = func.toString().replace(stripComments, '');
        var result = fnStr.slice(fnStr.indexOf('(') + 1, fnStr.indexOf(')')).match(argumentNames);
        if (result === null)
            result = [];
        return result;
    }

    this.globalServiceInstance = function (name, serviceInstance) {
        if (globalScope[name])
            throw new Error("Service '" + name + "' already registered");

        globalScope[name] = serviceInstance;
    };

    this.service = function (name, service, parametersNames, throwIfAlreadyExists) {
        if (serviceDescriptions[name]) {
            if (throwIfAlreadyExists)
                throw new Error("Service '" + name + "' already registered");

            return;
        }

        serviceDescriptions[name] = {
            name: name,
            service: service,
            parameters: parametersNames
        };
    };

    this.singleton = function (name, service, parametersNames, throwIfAlreadyExists) {
        if (globalServiceDescriptions[name]) {
            if (throwIfAlreadyExists)
                throw new Error("Service '" + name + "' already registered");

            return;
        }

        globalServiceDescriptions[name] = {
            name: name,
            service: service,
            parameters: parametersNames
        };
    };

    function createInstanceWithArguments(fConstructor, aArgs) {
        var foo = Object.create(fConstructor.prototype);
        fConstructor.apply(foo, aArgs);
        return foo;
    }

    var getParameters = function (service, scope, parametersNames) {
        scope = scope || globalScope;
        var functionParameterNames = getParamNames(service);

        parametersNames = parametersNames || [];
        if (checkFunctionNames) {
            var raiseError = function() {
                throw new Error("Function inject parameters list (" + parametersNames.join(", ") + ") different from defined arguments (" + functionParameterNames.join(", ") + "). Function is: " + service.toString());
            };
            if (functionParameterNames.length != parametersNames.length) raiseError();
            for (var j = 0; j < parametersNames.length; j++) {
                if (functionParameterNames[j].length != parametersNames[j].length) raiseError();
            }
        }

        var parameterInstances = [];
        for (var i = 0; i < parametersNames.length; i++) {
            var parameter = parametersNames[i];
            var parameterInstance = parameter == "scope" ? scope : self.get(parameter, scope);
            parameterInstances.push(parameterInstance);
        }
        return parameterInstances;
    }

    this.callResolved = function (service, context, scope, parametersNames) {
        var params = getParameters(service, scope, parametersNames);

        // ReSharper disable once InconsistentNaming
        var instance = service.apply(context, params);
        return instance;
    };

    this.resolve = function (service, scope, parameters) {
        var params = getParameters(service, scope, parameters);

        // ReSharper disable once InconsistentNaming
        var instance = createInstanceWithArguments(service, params);
        return instance;
    };

    this.get = function (name, scope) {
        scope = scope || globalScope;

        var instance = globalScope[name] || scope[name];
        
        if (instance) return instance;

        var globalServiceDesc = globalServiceDescriptions[name];
        if (globalServiceDesc) {
            instance = self.resolve(globalServiceDesc.service, scope, globalServiceDesc.parameters);

            globalScope[name] = instance;

            return instance;
        }

        var serviceDesc = serviceDescriptions[name];
        if (!serviceDesc)
            throw new Error("Service '" + name + "' is not registered!");
        
        instance = self.resolve(serviceDesc.service, scope, serviceDesc.parameters);

        scope[name] = instance;

        return instance;
    };
}