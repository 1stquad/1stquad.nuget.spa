﻿/* Part of nuget package */
site.module(function() {
    "use strict";
    
    $(document).on("click", "form.ajax-form input[type=submit], form.ajax-form button[type=submit], form.ajax-form a.form-submit", function (e) {
        var input = $(this);
        var form = input.parents("form");
        form.data("submit-input", this);

        var eventArgs = {
            originalEvent: e,
            preventDefaultFormProcessing: false,
            form: form
        };

        input.trigger("beforeSubmit", [eventArgs]);

        if (eventArgs.preventDefaultFormProcessing)
            return;

        var name = input.attr("name") || input.attr("data-name");
        var value = input.attr("value") || input.attr("data-value");
        if (name && value) {
            var hiddenInput = form.children("input[type='hidden'][name='" + name + "']");
            if (!hiddenInput.size())
                hiddenInput = $("<input />").attr("type", "hidden").attr("name", name).attr("temp", "true").appendTo(form);
            hiddenInput.val(value);
        }
        if (input.is("a")) {
            e.preventDefault();
            form.submit();
        }
    });
    
    $(document).on("keyup keypress", "form input", function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
        return true;
    });

    $(document).on("submit", "form.ajax-form", function (e) {
        e.preventDefault();

        var form = $(this);

        var data = form.serializeArray(false);

        var url = form.attr("action");
        var submitInput = form.data("submit-input");
        if (submitInput) {
            if ($(submitInput).data("url")) {
                url = $(submitInput).data("url");
            }
            if ($(submitInput).attr("href")) {
                url = $(submitInput).attr("href");
            }

            var submitData = $(submitInput).data();
            $.each(submitData, function (key, value) {
                if (key.indexOf("form") === 0) {
                    var formField = key.substring("form".length);
                    data.push({ name: formField, value: value });
                }
            });
        }

        var eventArgs = {
            originalEvent: e,
            preventDefaultFormProcessing: false,
            form: form,
            data: data
        };

        form.trigger("onSubmit", [eventArgs]);

        if (eventArgs.preventDefaultFormProcessing)
            return false;

        site.loadPage({
            context: form,
            target: form,
            initiator: "form",
            responseFilter: function ($dom) {
                if (!($dom instanceof jQuery))
                    return $dom;

                var form = $dom.find("form.ajax-form").addBack().filter("form.ajax-form");
                return form;
            },
            replaceTarget: true,
            url: url,
            method: 'POST',
            data: data
        });

        return false;
    });
});