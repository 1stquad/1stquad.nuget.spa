﻿/* Part of nuget package */
site.module(["notifier"], function (notifier) {
    $.ajaxSetup({
        traditional: true,
        cache: false,
        error: function (jqXhr, exception) {
            var responseText = jqXhr.responseText || jqXhr.statusText || "Unknown AJAX error";

            var body = responseText.replace(/^[\S\s]*<body[^>]*?>/i, "").replace(/<\/body[\S\s]*$/i, "");

            var errorBody = $("<div>" + body + "</div>");

            notifier.error("Error in request, error code = " + jqXhr.status);

            if (jqXhr.status === 0) {
                //do nothing because it occurs if user stops loading of page. 
                //In some special cases (if headers are not strings in IE) it may be caused by incorrect XmlHttpRequest parameters
                //notifier.displayError('Not connected.\n Verify Network.', errorBody);
            } else if (jqXhr.status == 404) {
                notifier.displayError('Requested page not found. [404]', errorBody);
            } else if (jqXhr.status == 500) {
                notifier.displayError('Internal Server Error [500].', errorBody);
            } else if (exception === 'parsererror') {
                notifier.displayError('Requested JSON parse failed.', errorBody);
            } else if (exception === 'timeout') {
                notifier.displayError('Time out error.', errorBody);
            } else if (exception === 'abort') {
                notifier.displayError('Ajax request aborted.', errorBody);
            } else {
                notifier.displayError('Uncaught Error.\n' + jqXhr.responseText, errorBody);
            }
        }
    });
});