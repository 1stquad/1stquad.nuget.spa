﻿/* Part of nuget package */
site.module(function() {
    $(document).on("keyup keypress", "form input", function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            return false;
        }
        return true;
    });
});