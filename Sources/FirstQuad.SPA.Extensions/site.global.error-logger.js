﻿/* Part of nuget package */
site.module(["notifier"], function(notifier) {
    "use strict";

    window.onerror = function(msg, url, line, col, e) {
        "use strict";

        var href = location.href;

        if (e && (!e.stack)) {
            e = null;
        }

        if (e) {
            msg = e.toString();
            url = e.fileName;
            line = e.lineNumber;
        }

        if (notifier && msg) {
            var message = "Message: " + msg;
            if (url) {
                message += ", url: " + url;
            }

            message += ", href: " + href;

            if (line) {
                message += "line: " + line;
            }

            if (e) {
                message += '<br/>' + "CallStack: " + e.stack.replace(/(\r\n|\n\r|\n)/gm, "<br />");
            }
            notifier.error(message);
        }

        var tokens = $('input[name=__RequestVerificationToken]');
        var tok = "";
        if (tokens.length) {
            tok = tokens[0].value;
        }
        var out = 'ErrorMessage: ' + msg + ' ';
        if (url) {
            out += ", url: " + url;
        }

        out += ", href: " + href;

        if (line) {
            out += ', line: ' + line;
        }

        if (e && e.stack) {
            out += ", CallStack: " + e.stack;
        }

        var jsLogger = $("body").data("js-logger");
        if (jsLogger) {
            // send error message
            $.ajax({
                type: 'POST',
                url: $("body").data("js-logger"),
                data: { message: out, __RequestVerificationToken: tok }
            });
        }
    }
});
