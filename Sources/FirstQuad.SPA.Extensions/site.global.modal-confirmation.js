﻿/* Part of nuget package */
site.module(["modals"], function (modals) {
    "use strict";


    $(document).on("beforeSubmit", "form.ajax-form input[type=submit], form.ajax-form button[type=submit]", function (e, args) {
        var input = $(this);
        var form = args.form;

        if (input.hasClass("ajax-confirmation-action")) {
            args.originalEvent.preventDefault();
            args.preventDefaultFormProcessing = true;

            if (!form.valid())
                return;

            var confirmationModal = input.parents(".modal");
            var confirmationObjectId = confirmationModal.data("confirmation-for");
            var confirmationForm = confirmationModal.find("form");

            //update data on main form (hidden inputs)
            var updateForm = function (ownerForm) {

                var hasOwnerForm = confirmationForm[0] !== ownerForm[0];

                ownerForm.on("ajax-response", function () {
                    modals.hideModal(confirmationModal);
                });

                if (hasOwnerForm) {
                    var newElements = {};
                    $("input,textarea,select", form).each(function () {
                        var element = $(this);

                        var ownerInput = ownerForm.find("[name='" + this.name + "']");
                        if (!ownerInput.length || newElements[this.name]) {

                            var append = true;
                            if (element.attr("type") == "checkbox" && !element.is(":checked")) {
                                append = false;
                            }

                            if (append) {
                                newElements[this.name] = true;
                                ownerInput = $('<input type="hidden" class="hidden-form-confirmation-element"/>').attr("name", this.name).val(element.val()).appendTo(ownerForm);
                            }
                        }

                        ownerInput.val(element.val());
                    });
                }

                //add new submit button
                ajaxIndicator.show(input);
                var submitBtn = $('<input type="submit" name="' + input.attr("name") + '" value="' + input.val() + '" data-url="' + input.data("url") + '" class="hidden cancel" />').appendTo(ownerForm);
                submitBtn.data("confirmation-form", form);
                submitBtn.click();
            };

            //look for modal
            var mainForm = null;
            $(".modal").each(function () {
                var modalId = $(this).attr("uniqid");
                if (modalId == confirmationObjectId) {
                    mainForm = $(this).find("form");
                }
            });

            //look for form
            $("form").each(function () {
                var formId = $(this).data("form-id");
                if (formId == confirmationObjectId) {
                    mainForm = $(this);
                }
            });

            if (mainForm) {
                updateForm(mainForm);
            } else {
                throw new Error("Can't find main form for confirmation!");
            }

            return;
        }
    });

    site.addResponseHandler(function (data) {

        if (data.AjaxModalConfirmation === "true") {
            modals.closeActiveModal(this, data);
            var modal = modals.loadModalWindow(data.ViewHtml);
            modal.attr("data-confirmation-for", data.AjaxModalConfirmationFor);
            return true;
        }

        return false;
    });
});