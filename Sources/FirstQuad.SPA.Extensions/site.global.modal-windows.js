﻿/* Part of nuget package */
site.module("modals", function () {
    "use strict";

    var allowExternalComponentsLikePickersToBeFocusableWhileModalIsVisible = function () {
        $.fn.modal.Constructor.prototype.enforceFocus = function () {
            var focusHandler = function (e) {
                if (document !== e.target && this.$element[0] !== e.target && !this.$element.has(e.target).length && !$(e.target).closest(".dropdown-content").length) {
                    this.$element.trigger('focus');
                }
            };

            $(document)
                .off('focusin.bs.modal') // guard against infinite focus loop
                .on('focusin.bs.modal', $.proxy(focusHandler, this));
        };
    }

    var cleanUpModalhtml = function () {
        $(".modal:hidden").remove();
        if (!$(".modal:visible").length) {
            $("html").removeClass("no-scroll");
        }
    };

    var hideModal = function ($modal) {
        $modal.modal("hide");
    }

    var applyModalSuccess = function (modalWindow) {
        var sender = modalWindow.data("call-back-sender");
        if (modalWindow.data("call-back"))
            site.callFunction(sender, modalWindow.data("call-back"));
    };

    var closeActiveModal = function (sender, data) {
        var modalWindow = $(sender).parents(".modal");
        applyModalSuccess(modalWindow);

        if (data.OpenModalWindow) {
            site.loadPage({
                url: data.OpenModalWindow
            });
        }

        modalWindow.find("form").data("read-only", null);
        hideModal(modalWindow);
    };

    var loadModalWindow = function (html, context) {
        cleanUpModalhtml();

        var holder = $("body");
        var $dom = $(html);

        $dom.find(".modal").addBack().filter(".modal").each(function () {
            var mWnd = $(this).modal({
                show: true,
                backdrop: "static"
            });
            var uniqueId = Math.random().toString(32).slice(2);
            mWnd.attr("uniqid", uniqueId);
            mWnd.on('hidden.bs.modal', function () {
                cleanUpModalhtml();
            });
            var autofocusElement = mWnd.find("[autofocus]:first");
            if (autofocusElement.length) {
                try {
                    setTimeout(function() {
                        autofocusElement.focus(); 
                    },0);
                } catch (ex) {}
            }
            mWnd.data("call-back-sender", context);
            $("html").addClass("no-scroll");
        });

        holder.append($dom);
        site.applyInitScripts($dom);

        return $dom;
    };

    var closeModals = function () {
        $(".modal").not(".permanent-modal").each(function () {
            hideModal($(this));
        });
    };

    $(document).on("click", "a.ajax-modal-window", function (e) {
        e.preventDefault();
        var link = $(this);

        var url = link.attr("href");

        site.loadPage({
            context: link,
            url: url,
            disableHistory: true
        });
    });

    $(document).on("onSubmit", "form.ajax-form", function (e, args) {
        var form = args.form;
        var data = args.data;

        var modal = form.parents(".modal");
        if (modal.length)
            data.push({ name: "modalid", value: modal.attr("uniqid") });

        if (form.data("form-id"))
            data.push({ name: "formid", value: form.data("form-id") });
    });

    site.addResponseHandler(function (data, options) {
        var isFormPostBack = options.context && options.context && options.context.hasClass("ajax-form");
        if (data instanceof jQuery) {
            var hasModals = data.find(".modal").addBack().filter(".modal").length;

            if (hasModals && !isFormPostBack) {
                loadModalWindow(data, options.context);
                return true;
            }
        }

        if (isFormPostBack) {
            $(options.context).trigger("ajax-response");
        }

        var modalWindow;
        var caller;
        if (data.AjaxModal === "true") {
            modalWindow = $(this).parents(".modal");
            caller = modalWindow.data("call-back-sender");
            closeActiveModal(this, data);
            if (data && data.CallCustomJs) {
                site.callFunction(this, data.CallCustomJs, [data.Data, caller]);
            }
            
            return true;
        }

        if (data.AjaxNewModal === "true") {
            loadModalWindow(data.ViewHtml, options.context);
            return true;
        }

        if (data.AjaxModalFormSuccessWithDataJs === "true") {
            modalWindow = $(this).parents(".modal");
            caller = modalWindow.data("call-back-sender");
            closeActiveModal(this, data);
            site.callFunction(this, data.CallCustomJs, [data.Data, caller]);
            return true;
        }

        if (data.AjaxModalFormResultWithJs === "true") {
            var result;
            if (data.IsJson) {
                var responseData = $.parseJSON(data.Data);
                result = site.processAjaxData(responseData, options);
            } else {
                var $dom = $(data.Data).find("form");
                options.target = this;
                options.replaceTarget = true;
                result = site.updateBlock($dom, options);
            }

            if (data.CallCustomJs) {
                site.callFunction(options.context, data.CallCustomJs, [result, data.CustomJsData]);
            }
            return true;
        }

        return false;
    });

    allowExternalComponentsLikePickersToBeFocusableWhileModalIsVisible();

    this.loadModalWindow = loadModalWindow;
    this.closeModals = closeModals;
    this.hideModal = hideModal;
    this.closeActiveModal = closeActiveModal;

}).asSingleton();